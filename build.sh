#!/usr/bin/env bash

# Start time
start=`date +%s`

if [ $# -eq 1 ]; then
	export SHADOW_CHANNEL=$1
fi

# Build the ISO
echo "----------------------"
echo "| Building the image |"
echo "----------------------"
echo ""

nix-build '<nixpkgs/nixos>' \
	-A config.system.build.isoImage \
	-I nixos-config=default.nix \
	--show-trace

end=`date +%s`
runtime=$((end-start))
size=`du -sh result/iso/Shadow-LiveOS.iso | cut -f1`

# Display time and size of the image
echo "Execution time: $runtime seconds"
echo "Size of the image: $size"
