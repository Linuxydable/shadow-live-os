{ config, pkgs, lib, ... }:

with lib;

{
  # Network 
  networking.hostName = "Shadow-LiveOS";
  networking.wireless.enable = false;
  networking.networkmanager.enable = true;

  # Packages
  environment.systemPackages = with pkgs; [
    gnome3.networkmanagerapplet
    networkmanager
  ];
}
