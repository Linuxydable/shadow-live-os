{ config, ... }:

{
  # New style
  console.keyMap = "fr";
  console.font = "Lat2-Terminus16";

  # Set your time zone.
  time.timeZone = "Europe/Paris";
}
