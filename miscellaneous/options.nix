{ config, pkgs, lib, ... }:

with lib;
with types;

{
  # Openbox Autostart configuration
  options.openbox.autostartLines = mkOption {
    type = lib.types.listOf lib.types.str;
    default = [ ];
    description = "Lines to add to autostart";
  };
}
