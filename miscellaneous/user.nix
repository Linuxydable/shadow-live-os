{ config, pkgs, ... }:

{
  # We create a base user that will be automatically logged in at boot
  users = {
    mutableUsers = false;
    users.shadow = {
      description = "Shadow Live User";
      isNormalUser = true;
      home = "/home/shadow";
      createHome = true;
      extraGroups = [ "wheel" "networkmanager" "video" "audio" "tty" ];
      initialPassword = "";
    };
  };

  # Enable sudo
  security.sudo = {
    enable = true;
    wheelNeedsPassword = pkgs.lib.mkForce false;
  };

  # Workaround: creates the user folder in the Nix storage
  systemd.services."before-home-manager-shadow" = {
    script = "mkdir -p /nix/var/nix/profiles/per-user/shadow";
    path = [ pkgs.coreutils ];
    before = [ "home-manager-shadow.service" ];
    wantedBy = [ "multi-user.target" ];
  };
}
