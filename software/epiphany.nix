{ config, pkgs, ... }:

{
  # # Install Epiphany
  # environment.systemPackages = with pkgs; [ epiphany ];

  # # Fixes a bug where nothing is rendered (#19)
  # environment.variables.WEBKIT_DISABLE_COMPOSITING_MODE = "1";

  # # Populates the home folder with default files
  # home-manager.users.shadow.dconf.settings = {
  #   "org/gnome/epiphany/web" = {
  #     ask-on-download = true;
  #     enable-mouse-gestures = true;
  #     remember-passwords = false;
  #   };
  #   "org/gnome/epiphany" = {
  #     homepage-url = "https://gitlab.com/NicolasGuilloux/shadow-live-os";
  #   };
  # };
}
