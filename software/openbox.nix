{ config, pkgs, lib, ... }:

let
  autostart = lines:
    pkgs.writeShellScriptBin "autostart"
    (lib.strings.concatStringsSep (" & " + "\n") lines);
in {
  # Light window manager
  services.xserver.windowManager.openbox.enable = true;
  services.xserver.displayManager.defaultSession = "none+openbox";

  # Enable DConf
  programs.dconf.enable = true;

  # Openbox Configuration
  environment.etc = {
    "xdg/openbox/menu.xml".source = (pkgs.callPackage ../assets/obmenu.nix { });
    "xdg/openbox/rc.xml".source = ../assets/openbox_config.xml;
    "xdg/openbox/autostart".source =
      "${autostart config.openbox.autostartLines}/bin/autostart";
  };

  home-manager.users.shadow.home.file.".config/openbox/autostart".source =
    "${autostart config.openbox.autostartLines}/bin/autostart";

  # Packages
  environment.systemPackages = with pkgs; [ openbox-menu numlockx lxrandr ];

  # Add NmApplet and Numlockx at startup
  openbox.autostartLines =
    [ "${pkgs.numlockx}/bin/numlockx" "${pkgs.networkmanager}/bin/nm-applet" ];
}
