{ config, pkgs, ... }:

{
  # Autolog shadow user
  services.mingetty.autologinUser = "shadow";

  # Aliases
  environment.shellAliases = {
    "report" =
      "curl https://raw.githubusercontent.com/NicolasGuilloux/blade-shadow-beta/master/scripts/report.pl | perl";
  };

  # Packages
  environment.systemPackages = with pkgs; [ alacritty ];
}
