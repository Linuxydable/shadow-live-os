{ config, pkgs, ... }:

{
  # Packages
  environment.systemPackages = with pkgs;
    [
      # numix-solarized-gtk-theme
      hicolor-icon-theme
    ];

  home-manager.users.shadow.gtk = {
    enable = true;
    font.package = pkgs.fira-code;
    font.name = "Fira Code";
    iconTheme.package = pkgs.numix-icon-theme;
    iconTheme.name = "Numix";
    theme.package = pkgs.matcha-gtk-theme;
    theme.name = "Matcha-aliz";
    gtk2.extraConfig = ''
      gtk-toolbar-style=GTK_TOOLBAR_BOTH_HORIZ
      gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR
      gtk-xft-antialias=1
    '';
  };
}
